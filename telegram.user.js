// ==UserScript==
// @id             iitc-plugin-telegram
// @name           IITC plugin: Englithened Telegram Layer
// @category       Layer
// @version        0.1.0.20171016
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://$$host$$/telegram.user.js
// @downloadURL    https://$$host$$/telegram.user.js
// @description    Telegram Layer for IITC - View Telegram users on IITC map
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {
    // ensure plugin framework is there, even if iitc is not yet loaded
    if(typeof window.plugin !== 'function')
       window.plugin = () => {};

    window.plugin.telegramViewer = () => {};

    window.plugin.telegramViewer.members = [];
    window.plugin.telegramViewer.NAME_HEIGHT = 23;
    window.plugin.telegramViewer.NAME_WIDTH = 80;

    // BASED ON https://github.com/3ch01c/iitc-plugins/blob/master/glympse.user.js

	window.plugin.telegramViewer.setupCSS = function() {
	    $("<style>").prop("type", "text/css").html([
	  		'.plugin-telegram-names{',
				'color:#FFFFBB;',
				'font-size:11px;line-height:12px;',
				'text-align:center;padding: 2px;',
				'overflow:hidden;',
				'text-shadow:1px 1px #000,1px -1px #000,-1px 1px #000,-1px -1px #000, 0 0 5px #000;',
				'pointer-events:none;',
		  	'}',
  		].join('\n')).appendTo("head");
	};

    const telegramIcon = L.icon({
        iconUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAoCAMAAADNGvrUAAAACXBIWXMAAAAcAAAAHAAPAbmPAAABDlBMVEUAAAC3PDC7PDC3ODDHQDC7PDC3ODDbSEi3ODC3PDC7PDC3PDC/QDS7PDDTVFTDRDi3PDS7PDC7PDS7PDTbXEjHSDi7PDC7QDS7ODC3ODDLVEC3ODC3PDC/PDTDPDC3ODC3ODDrZFjLPDS7PDDDRDzzUEj7cGTfXFS3ODD3bGD3ZFj3XFA0AADLTEDzTET7dGjbTEDTQDSXNCzzSDzbVEzzSEDTSEC7ODDTUEjXVEz3aFz3YFj3YFT3XFT3WEz3VEjTVExkHBh8KCT3VEz3WFDbUETnTEDjTEDnSEDzUETjSEDnRDjjRDjbRDiLLCjfRDyvRDzbTER8JCCXNDDTTEDTSDzLSEDXVEhkGBRkGBitvd3zAAAAIXRSTlMAt3TjJJf7Cu/3gNNEyw44i2CA6xAU62Skxxj3yzws89MrbPY6AAABTklEQVR42u39VXLDMBCAYawxsR0OByvZ4djhQLlcLhfuf5EqkTzGK/xPO9/Lzqx0AKBVeC4ry1mOr4BTIqWYHdRuo46ppBK2iprZG7F633siU2EzcrURdh6J/7Q9reMRwrzR9mXwALkm8jNq5qBo9AIZReDmQZ5zUED7NHR+9mqPBZD7bP4YDoendOzL0LX5j/CUcRcU1Kc9E17TESmgdRij998pGzsaSJ8HgZYSJPXDQHoSoHbl17cauUlMHxx5Guix7Qm55cDTF0fvXW1dumpV2TtkjGNXOMMeLSKMHR0LEfsx63rrgtXS687TS/iEhSXXj8hHr6neRPMuBh63dmHerWTr7R3p3tlHU7FFwqpXQZS3LIs+hvKLZT2V/UqWLha+hdsaq9ls1QgwCA+PQlChNJmUQjiN9XQIi92uGMIQjYYpcFwoS1Ioq66D/AMaPlsPt9lyzAAAAABJRU5ErkJggg==',
        iconSize:     [22, 40],
        iconAnchor:   [11, 39],
        popupAnchor:  [-3, -76]
    });

    window.plugin.telegramViewer.loadChat = async (chat_id) => {
        console.log(`[telegram] loading chat ${chat_id}`);
        const url = `https://$$host$$/chat/${chat_id}`;
        const response = await (fetch(url).then((r) => { return r.json(); }));

        const socket = io('https://$$host$$');
        socket.on('connect', () => {
            console.log('[telegram] connected');

            socket.emit('subscribe', {
                chat_id: chat_id,
            });
        });
        socket.on('location', (user) => {
            console.log('[telegram] location update ' + JSON.stringify(user));
            const latLng = new L.LatLng(user.latitude, user.longitude);

            if(!window.plugin.telegramViewer.members[user.id]) {
                window.plugin.telegramViewer.members[user.id] = {};
                const marker = L.marker(latLng, {
                    icon: telegramIcon,
                    riseOnHover: true,
                    html: user.name,
                });//.bindPopup(popupContent);
                window.plugin.telegramViewer.members[user.id].marker = marker;
                marker.on('popupopen', function() {
                    $(this._icon).toggleClass('selected', true);
                });
                marker.on('popupclose', function() {
                    $(this._icon).toggleClass('selected', false);
                });
                marker.addTo(window.plugin.telegramViewer.telegramUsersGroup);

                const text = L.marker(latLng, {
                    icon: L.divIcon({
                        className: 'plugin-telegram-names',
                        iconAnchor: [window.plugin.telegramViewer.NAME_WIDTH / 2, 0],
                        iconSize: [window.plugin.telegramViewer.NAME_WIDTH, window.plugin.telegramViewer.NAME_HEIGHT],
                        html: user.name,
                    }),
                });//.bindPopup(popupContent);
                window.plugin.telegramViewer.members[user.id].text = text;
                text.addTo(window.plugin.telegramViewer.telegramUsersGroup);
            }

            window.plugin.telegramViewer.members[user.id].marker.setLatLng(latLng);
            window.plugin.telegramViewer.members[user.id].text.setLatLng(latLng);
       });
    };

    window.plugin.telegramViewer.init = () => {
        //$('#toolbox').append($('<a onclick="window.plugin.telegramViewer.loadChat(); return false;" accesskey="t" title="Load a telegram chat users location [t]">Telegram</a>'));

        window.plugin.telegramViewer.telegramUsersGroup = new L.LayerGroup();
        window.addLayerGroup('Telegram', window.plugin.telegramViewer.telegramUsersGroup, false);

		window.plugin.telegramViewer.setupCSS();

        console.log('[telegram] initalized');

        const query = window.location.search.substring(1);
        const queryArgs = query.split('&');
        for(const arg of queryArgs) {
            const [key, value] = arg.split('=');
            if(key === 'tc') {
                window.plugin.telegramViewer.loadChat(parseInt(value));
                break;
            }
        }
    };

    var setup = () => {
        window.addHook('iitcLoaded', window.plugin.telegramViewer.init);
    };

    setup.info = plugin_info;
    // add the script info data to the function as a property
    if(!window.bootPlugins)
        window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function')
        setup();
}
// wrapper end
// inject code into site context

var script = document.createElement('script');
var info = {
};
if(typeof GM_info !== 'undefined' && GM_info && GM_info.script) {
    info.script = {
    version: GM_info.script.version,
    name: GM_info.script.name,
    description: GM_info.script.description
    };
}
script.appendChild(document.createTextNode(`(${wrapper})(${JSON.stringify(info)});`));
(document.body || document.head || document.documentElement).appendChild(script);

function loadScriptByUrl(url) {
	var script = document.createElement('script');
	script.src = url;
	(document.body || document.head || document.documentElement).appendChild(script);

}
loadScriptByUrl('https://$$host$$/socket.io/socket.io.js');
