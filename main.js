const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const Telegram = require('./TelegramBot');
const fs = require('fs');
const config = JSON.parse(fs.readFileSync('config.json'));
const telegram = new Telegram(config.telegram);
const Pg = require('pg').Client;
const bodyParser = require('body-parser');
const format = require('pg-format');

async function queryOne(pg, sql, params) {
    const result = await pg.query(sql, params);
    if(result.rowCount === 0)
        return null;
    else
        return result.rows[0];
}

const subscriptions = {};

(async () => {
    const pg = new Pg(config.database);
    await pg.connect();

    const me = await telegram.getMe();
    console.log(me);

    telegram.on('message', async (message) => {
        if(message.location) {
            const created = new Date(message.date * 1000);
            const updated = message.edit_date ? new Date(message.edit_date * 1000) : created;
            const user = message.from;
            const chat = message.chat;

            console.log(`${updated} => ${message.from.username} at ${message.location.latitude},${message.location.longitude}`);

            // realtime update

            if(message.chat.id in subscriptions) {
                for(const listener of Object.values(subscriptions[message.chat.id])) {
                    console.log({
                        id: message.from.id,
                        name: message.from.username,
                        timestamp: updated,
                        latitude: message.location.latitude,
                        longitude: message.location.longitude,
                    });
                    listener.emit('location', {
                        id: message.from.id,
                        name: message.from.username,
                        timestamp: updated,
                        latitude: message.location.latitude,
                        longitude: message.location.longitude,
                    });
                }
            }

            // save location in database
            let sql = 'BEGIN;\n';
            sql += format([
                'INSERT INTO "Users" (id, name, created, updated)',
                'VALUES (%1$L, %2$L, %3$L, %4$L)',
                'ON CONFLICT ON CONSTRAINT "Users_id" DO UPDATE SET',
                'name = CASE WHEN %4$L > "Users".updated THEN EXCLUDED.name ELSE "Users".name END,',
                'updated = GREATEST(%4$L, "Users".updated);',
            ].join('\n') + '\n', message.from.id, message.from.username, created, updated);
            sql += format([
                'INSERT INTO "Chats" (id, name, created, updated)',
                'VALUES (%1$L, %2$L, %3$L, %4$L)',
                'ON CONFLICT ON CONSTRAINT "Chats_id" DO UPDATE SET',
                'name = CASE WHEN %4$L > "Chats".updated THEN EXCLUDED.name ELSE "Chats".name END,',
                'updated = GREATEST(%4$L, "Chats".updated);',
            ].join('\n') + '\n', message.chat.id, message.chat.title || message.chat.username, created, updated);
            sql += format([
                'INSERT INTO "Locations" (chat, "user", created, updated, geo)',
                'VALUES (%1$L, %2$L, %3$L, %4$L, ST_MakePoint(%6$L, %5$L))',
                'ON CONFLICT ON CONSTRAINT "Locations_chat_user" DO UPDATE SET',
                'geo = CASE WHEN %4$L > "Locations".updated THEN EXCLUDED.geo ELSE "Locations".geo END,',
                'updated = GREATEST(%4$L, "Locations".updated);',
            ].join('\n') + '\n', message.chat.id, message.from.id, created, updated, message.location.latitude, message.location.longitude);
            sql += 'COMMIT;\n';
            await pg.query(sql);
        }
        else {
            if(message.entities && message.entities.length > 0 && message.entities[0].type === 'bot_command') {
                if(message.text === `/location@${me.username}`) {
                    const locations = await pg.query('SELECT "user", name, l.created, l.updated, ST_Y(geo::geometry) as latitude, ST_X(geo::geometry) as longitude FROM "Locations" as l INNER JOIN "Users" as u ON u.id = "user" WHERE chat = $1', [message.chat.id]);

                    let text = `<a href="https://www.ingress.com/intel?tc=${message.chat.id}">intel</a> (<a href="https://${config.host}/telegram.user.js">IITC plugin</a>)\n`;
                    if(locations.rowCount > 0) {
                        text += locations.rows.map((loc) => {
                            return `<b>${loc.name}</b>: <a href="https://maps.google.com/?q=${loc.latitude},${loc.longitude}">${loc.latitude},${loc.longitude}</a> (<a href="https://www.ingress.com/intel?ll=${loc.latitude},${loc.longitude}&z=15">intel</a>) at ${loc.updated}`;
                        }).join('\n');
                    }

                    telegram.sendMessage({
                        chat_id: message.chat.id,
                        parse_mode: 'html',
                        disable_web_page_preview: true,
                        text: text,
                    });
                }
            }
        }
    });

    app.use(bodyParser.json());

    app.get('/chat/:chat_id', async (req, res, next) => {
        if(req.headers.origin && /^https:\/\/(www\.|)ingress\.com$/i.test(req.headers.origin)) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

            const chat = await queryOne(pg, 'SELECT * FROM "Chats" WHERE id = $1 LIMIT 1', [req.params.chat_id]);
            if(chat) {
                const locations = await pg.query('SELECT "user"::integer as id, name, l.created, l.updated, ST_Y(geo::geometry) as latitude, ST_X(geo::geometry) as longitude FROM "Locations" as l INNER JOIN "Users" as u ON u.id = "user" WHERE chat = $1', [req.params.chat_id]);

                res.json({
                    chat: chat,
                    locations: locations.rows,
                });
                return;
            }
        }
        next();
    });

    app.get('/', (req, res) => {
        res.send('<script src="/socket.io/socket.io.js"></script><a href="/telegram.user.js">iitc plugin</a>');
    });

    app.get('/telegram.user.js', (req, res) => {
        fs.readFile(__dirname + '/telegram.user.js', 'utf8', (err, source) => {
            source = source.replace(/\$\$host\$\$/g, config.host);
            res.setHeader('Content-Type', 'application/javascript');
            res.send(source);
        });            
    });

    let connectionCount = 0;
	io.on('connection', function(socket){
        const id = ++connectionCount;

        console.log(`client#${id}: connected`);

		socket.on('subscribe', async (msg) => {
            const chat = msg.chat_id;

            console.log(`client#${id}: subscribed to chat ${chat}`);

            // subscribe
            if(!subscriptions[chat])
                subscriptions[chat] = {};
            subscriptions[chat][id] = socket;

            // send old known locations
            const locations = await pg.query('SELECT "user"::integer as id, name, l.updated as timestamp, ST_Y(geo::geometry) as latitude, ST_X(geo::geometry) as longitude FROM "Locations" as l INNER JOIN "Users" as u ON u.id = "user" WHERE chat = $1', [chat]);
            locations.rows.forEach((user) => {
                socket.emit('location', user);
            });
		});

        socket.on('disconnect', function(){
            for(const chat of Object.values(subscriptions)) {
                if(id in chat) {
                    console.log(`client#${id}: unsubscribed from chat ${chat}`);
                    chat[id] = undefined;
                    delete chat[id];
                }
            }
            console.log(`client#${id}: disconnected`);
        });
	});

    http.listen(config.port, () => {
        telegram.setWebhook(app, `/botapi/${config.telegram.token}`).then(() => {
            console.log('ready');
        });
    });
})();
