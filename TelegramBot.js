const http = require('request-promise-json');
const telegramBaseUrl = 'https://api.telegram.org';
const express = require('express');
const bodyParser = require('body-parser');

module.exports = class TelegramBot {
    constructor(config) {
        this.token = config.token;
        this.hookUrl = config.hook;
        this.router = new express.Router();
        this.events = {};
    }

    rpc(method, args) {
        return http.post(`${telegramBaseUrl}/bot${this.token}/${method}`, args);
    }

    on(name, f) {
        this.events[name] = f;
    }

    async getMe() {
        const response = await this.rpc('getMe');
        if(!response || !response.ok)
            throw "Failed to getMe";
        return response.result;
    }

    async sendMessage(args) {
        return await this.rpc('sendMessage', args);
    }

    setWebhook(app, endpoint) {
        return new Promise((resolve, reject) => {
            app.post(endpoint, (req, res) => {
                const message = req.body.message || req.body.channel_post || req.body.edited_message || req.body.edited_channel_post;
                if(message) {
                    this.events['message'](message);
                }
                res.json({ message: 'hooray! welcome to our api!' });   
            });

            this.rpc('setWebhook', {
                url: this.hookUrl,
                allowed_updates: ['message', 'edited_message', 'channel_post', 'edited_channel_post'],
            }).then((res) => {
                resolve();
            }).fail((e) => {
                console.error(e.response);
                reject(e);
            });
        });
    }

};
