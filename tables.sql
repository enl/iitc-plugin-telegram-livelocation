------------------------------------------------------------------------------
DROP TABLE IF EXISTS "Locations";
DROP TABLE IF EXISTS "Users";
DROP TABLE IF EXISTS "Chats";
DROP SEQUENCE IF EXISTS "Locations_id_seq";
------------------------------------------------------------------------------



------------------------------------------------------------------------------
CREATE TABLE "Users" (
    "id" integer NOT NULL,
    "name" text NOT NULL,
    "created" timestamptz NOT NULL,
    "updated" timestamptz NOT NULL,
    CONSTRAINT "Users_id" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "Users_name" ON "Users" USING btree ("name");
CREATE INDEX "Users_created" ON "Users" USING btree ("created");
CREATE INDEX "Users_updated" ON "Users" USING btree ("updated");
------------------------------------------------------------------------------



------------------------------------------------------------------------------
CREATE TABLE "Chats" (
    "id" bigint NOT NULL,
    "name" text NOT NULL,
    "created" timestamptz NOT NULL,
    "updated" timestamptz NOT NULL,
    CONSTRAINT "Chats_id" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "Chats_created" ON "Chats" USING btree ("created");
CREATE INDEX "Chats_updated" ON "Chats" USING btree ("updated");
------------------------------------------------------------------------------



------------------------------------------------------------------------------
CREATE SEQUENCE "Locations_id_seq" INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "Locations" (
    "id" integer DEFAULT nextval('"Locations_id_seq"') NOT NULL,
    "chat" bigint NOT NULL,
    "user" bigint NOT NULL,
    "created" timestamptz NOT NULL,
    "updated" timestamptz NOT NULL,
    "geo" geography,
    CONSTRAINT "Locations_id" PRIMARY KEY ("id"),
    CONSTRAINT "Locations_chat_user" UNIQUE ("chat", "user"),
    CONSTRAINT "Locations_chat_fkey" FOREIGN KEY ("chat") REFERENCES "Chats"(id) ON DELETE RESTRICT NOT DEFERRABLE,
    CONSTRAINT "Locations_user_fkey" FOREIGN KEY ("user") REFERENCES "Users"(id) ON DELETE RESTRICT NOT DEFERRABLE
) WITH (oids = false);

CREATE INDEX "Locations_geo" ON "Locations" USING gist ("geo");
CREATE INDEX "Locations_created" ON "Locations" USING btree ("created");
CREATE INDEX "Locations_updated" ON "Locations" USING btree ("updated");
------------------------------------------------------------------------------
